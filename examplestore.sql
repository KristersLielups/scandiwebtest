-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 30, 2018 at 09:24 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `examplestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `SKU` varchar(10) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Price` decimal(6,2) NOT NULL,
  `Type` enum('Book','CD','Furniture') NOT NULL,
  `Attribute` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`SKU`, `Name`, `Price`, `Type`, `Attribute`) VALUES
('BKK111111', 'ALL THE NAMES ARE RANDOM MOVIE NAMES', 111.00, 'Book', '111 Kg'),
('BKK37294', 'Beautiful Creatures', 1.83, 'Book', '73 Kg'),
('BKK67648', 'It\'s Always Fair Weather', 10.67, 'Book', '1.87 Kg'),
('BKK75450', 'Derrida', 233.75, 'Book', '80 Kg'),
('BKK89016', 'The Raid Redemption', 1.63, 'Book', '98 Kg'),
('CD21293', 'True Grit', 358.69, 'CD', '54 MB'),
('CD23832', 'Dan in Real Life', 365.96, 'CD', '84 MB'),
('CD65046', 'River Murders, The', 373.72, 'CD', '24 MB'),
('CD67648', 'Grand Dukes Les grands du', 86.07, 'CD', '48 MB'),
('FRN22570', 'Last of Mrs. Cheyney, The', 365.96, 'Furniture', '56x56x56 CM'),
('FRN77416', 'Liliom', 135.22, 'Furniture', '81x98x30 CM'),
('FRN89016', 'Cellular', 1.33, 'Furniture', '51x24x94 CM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`SKU`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
