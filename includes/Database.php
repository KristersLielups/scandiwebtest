<?php
include_once "validation.php";
/**
 * Class Database
 *
 * Creates a DB connection and deals with DB operations
 */
class Database{
    // Connection Parameters
    private $DB_HOST        = "localhost";
    private $DB_USER        = "root";
    private $DB_PASS        = "password";
    private $DB_DATABASE    = "examplestore";
    private $DB_PORT        = 3306;

    private $connection;
    private $statement;

    /**
     * Database constructor.
     *
     * Creates a connection to the Database using above defined parameters.
     *
     * $options contains additional PDO options
     *
     * Created PDO is stored in $connection
     */
    public function __construct(){
        // Database Source
        $dsn = "mysql:host=" . $this->DB_HOST . ";dbname=" . $this->DB_DATABASE . ";port=" . $this->DB_PORT;
        // Options
        $options = array(
            PDO::ATTR_PERSISTENT            => true,
            PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC
        );

        // Make Connection
        try {
            $this->connection = new PDO($dsn, $this->DB_USER, $this->DB_PASS, $options);
        } catch (PDOException $e){
            die($e->getMessage());
        }
    }

    // Prepare statement from query
    private function query($query){
        $this->statement = $this->connection->prepare($query);
    }

    /**
     * Fetches all products from the DB and returns an array of
     * associative arrays containing product information
     *
     * @return array
     */
    public function getProducts(){
        $this->query("SELECT * FROM products");
        $this->statement->execute();
        return $this->statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addProduct($SKU, $name, $price, $type, $attribute){
        $this->query("INSERT INTO `products`(`SKU`, `Name`, `Price`, `Type`, `Attribute`)
                      VALUES (:SKU, :Name, :Price, :Type, :Attribute)");
        $properties = array("SKU" => $SKU, "Name" => $name, "Price" => $price,
                                "Type" => $type, "Attribute" => $attribute);
        if (validateFormArray($properties)){
            return $this->statement->execute($properties);
        } else {
            return false;
        }

    }

    /**
     * Deletes specific product from DB
     * Product is identified by its SKU
     *
     * @param String $SKU
     */
    public function deleteProduct($SKU){
        $this->query("DELETE FROM products WHERE SKU = :SKU");
        //$this->bind(":SKU", $SKU);
        $this->statement->execute(array(":SKU" => $SKU));
    }
}