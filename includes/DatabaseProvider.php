<?php
include_once "Database.php";
 /**
  *  Database singleton
  *
  * Stores a single Database instance so it can be shared with other classes.
  */
class DatabaseProvider{

    private static $instance = null;

    /**
     * Return the stored Database instance.
     * Creates it if it hasn't been created yet.
     *
     * @return Database
     */
    public static function getConnection(){
        if (!self::$instance){
            self::$instance = new Database();
        }
        return self::$instance;
    }
}