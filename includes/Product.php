<?php
include_once "DatabaseProvider.php";
/**
 * Class Product
 *
 * Stores information about a product
 */
class Product{

    private $db;
    private $SKU, $name, $price, $type, $attribute;

    /**
     * Product constructor.
     *
     * Normally should be called my ProductFactory
     *
     * @param String $SKU
     * @param String $name
     * @param double $price
     * @param String $type
     * @param String $attribute
     */
    public function __construct($SKU, $name, $price, $type, $attribute)
    {
        $this->db = DatabaseProvider::getConnection();
        $this->SKU = $SKU;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
        $this->attribute = $attribute;
    }

    /**
     *  Deletes this product by sending this Product's SKU to the Database.deleteProduct method
     */
    public function deleteProduct(){
        $this->db->deleteProduct($this->SKU);
    }

    /**
     * Returns an associative array with the Products properties
     *
     * @return array
     */
    public function getProperties(){
        return array("SKU"      => $this->SKU,
                    "Name"      => $this->name,
                    "Price"     => $this->price,
                    "Type"      => $this->type,
                    "Attribute" => $this->attribute);
    }

}