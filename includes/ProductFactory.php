<?php
include_once "Product.php";

/**
 * Class ProductFactory
 *
 * Creates Products out of an array from a DB or from a single array with
 * Product properties
 */
class ProductFactory{

    /**
     * Takes an array containing arrays with product properties(DB output) and
     * creates a Product out of each of them. Stores them in $products and return
     * an array with constructed Products
     *
     * @param array    contains multiple rows from the DB
     * @return array
     */
    function createMany(array $DBProducts){
        //echo $DBProducts;
        $products = array();
        foreach ($DBProducts as $product){
            $products[$product["SKU"]] = new Product($product["SKU"], $product["Name"], $product["Price"],
                                                    $product["Type"], $product["Attribute"]);
        }
        return $products;
    }

    /**
     * Takes an array wit the properties of one Product. Creates a new Product
     * and returns it
     *
     * @param array    associative array with product details
     * @return Product
     */
    function create(array $properties){
        return new Product($properties["SKU"], $properties["Name"],
            $properties["Price"], $properties["Type"], $properties["Attribute"]);
    }
}