<?php
/**
 * Takes the $input array and checks if each stored values matches format.
 * If a value doesn't match, returns false, otherwise true
 *
 * @param array Form submit data
 * @return bool Valid or Invalid
 */
function validateFormArray(array $input){

    if (!(preg_match("/^[A-Z]{1,4}\d+$/", $input["SKU"]) && strlen($input["SKU"]) <= 10)){
        return false;
    }
    if (!(preg_match("/^[a-z\d\s'():,.]+$/i", $input["Name"]) && strlen($input["Name"]) <= 255)){
        return false;
    }
    if (!preg_match("/^(\d{1,6}([,.]\d{1,2})?)$/", $input["Price"])){
        return false;
    }

    if ($input["Type"] == "Book" || $input["Type"] == "CD"){
        if (!preg_match("/^(\d{1,6}([,.]\d{1,2})?)\s[a-zA-Z]{2}$/", $input["Attribute"])){
            return false;
        }
    } else if ($input["Type"] == "Furniture"){
        if (!preg_match("/^\d{1,3}x\d{1,3}x\d{1,3}\sCM$/", $input["Attribute"])){
            return false;
        }
    }

    return true;
}