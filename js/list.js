/**
 * Finds all checked checkboxes and deletes the products
 * that mach the checked checkbox values
 */
function massDelete() {
    var checked = [];
    $(".property_checkbox").each(function () {
        if ($(this).is(":checked")) {
            var val = $(this).val();
            checked.push(val);
            $("#" + val).remove();
        }
    });
    $.post("list.php", { "deleteSKUs" : checked });
}