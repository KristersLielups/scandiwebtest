var bookDiv, cdDiv, furnitureDiv;

// Remove and Store all attribute Divs
$(document).ready( function () {
    bookDiv = $(".book").detach();
    cdDiv = $(".cd").detach();
    furnitureDiv = $(".furniture").detach();
});

/**
 * Removes all attribute Divs
 */
function detachAttributes() {
    $(".book").detach();
    $(".cd").detach();
    $(".furniture").detach();
}

/**
 * Gets the selected Type and displays the appropriate
 * Div with the correct input fields
 */
function updateAttributes() {
    // Get selected Type
    var value = $("select").find(":selected").val();
    var skuprefix = $("#SKUPrefix");

    // Remove all attribute Divs, add required Div, change SKU prefix
    if (value === "Book"){
        detachAttributes();
        skuprefix.text("BKK");
        $(".properties").append(bookDiv);
    } else if (value === "CD"){
        detachAttributes();
        skuprefix.text("CD");
        $(".properties").append(cdDiv);
    } else {
        detachAttributes();
        skuprefix.text("FRN");
        $(".properties").append(furnitureDiv);
    }
}

/**
 * Validates all form data and cancels submission if
 *  at least one field does't pass validation
 *
 * @param event     Event sent from form submit action
 * @returns {boolean}
 */
function validateForm(event) {
    // Fetch fields
    var SKU = $("form input[name='SKU']");
    var Name = $("form input[name='Name']");
    var Price = $("form input[name='Price']");
    var Type = $("form select").find(":selected").val();
    var Attribute = $("form input[name='Attribute']");


    // Build SKU and Validate (  AZ#+  and <= 10)
    var skuprefix = $("#SKUPrefix").text();
    var fullSKU = skuprefix + SKU.val();
    if(/^[A-Z]{1,4}\d+$/.test(fullSKU) && fullSKU.length <= 10){
        SKU.removeClass("invalid");
    } else {
        SKU.addClass("invalid");
        event.preventDefault();
        return false;
    }

    // Validate Name (  iz AlphaNumeric and <= 255  )
    if (/^[a-z\d\s'():,.]+$/i.test(Name.val()) && Name.val().length <= 255){
        Name.removeClass("invalid");
    } else {
        Name.addClass("invalid");
        event.preventDefault();
        return false;
    }

    // Validate Price (  #{1,6}([,.]##?)?  );
    if (/^(\d{1,6}([,.]\d{1,2})?)$/.test(Price.val())){
        Price.removeClass("invalid");
    } else {
        Price.addClass("invalid");
        event.preventDefault();
        return false;
    }

    // Validate Attribute
    var valid = false;
    /* Book and CD */
    if (Type === "Book" || Type === "CD"){
        var fullAttribute = Attribute.val() + " " + $("#AttrSuffix").text();
        if (/^(\d{1,6}([,.]\d{1,2})?)\s[a-zA-Z]{2}$/.test(fullAttribute)){
            valid = true;
        }
    }
    /* Furniture */
    else if (Type === "Furniture"){
        var H = $("form input[name='H']");
        var W = $("form input[name='W']");
        var L = $("form input[name='L']");
        var fullAttribute = H.val() + "x" + W.val() + "x" + L.val() + " CM";
        if (/^\d{1,3}x\d{1,3}x\d{1,3}\sCM$/.test(fullAttribute)){
            H.removeClass("invalid"); W.removeClass("invalid"); L.removeClass("invalid");
            valid = true;
        } else {
            H.addClass("invalid"); W.addClass("invalid"); L.addClass("invalid");
        }
    } else {
        return false;
    }

    // Finalize
    if (valid){
        Attribute.removeClass("invalid");
        SKU.val(fullSKU);               // Make sure full SKU is sent in POST
        Attribute.val(fullAttribute);   // Make sure full Attribute is sent in POST
    } else {
        Attribute.addClass("invalid");
        event.preventDefault();
        return false;
    }
}