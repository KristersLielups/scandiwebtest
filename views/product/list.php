<?php include_once "../../includes/DatabaseProvider.php" ?>
<?php include_once "../../includes/ProductFactory.php" ?>
<?php include_once "../../includes/Product.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Product list</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet">
    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/list.js"></script>
</head>
<body>
    <!--Nav Bar-->
    <nav class="navbar">
        <div class="float-left">
            <a class="navbar-brand active" href="list.php">Product List</a>
            <a class="navbar-brand" href="new.php">Add Product</a>
        </div>
        <div class="float-right">
            <button class="nav_button" onclick="massDelete()">Mass Delete</button>
        </div>
    </nav>

    <!--Product Window-->
    <div class="product_window">
    <?php
        $db = DatabaseProvider::getConnection();
        $factory = new ProductFactory();
        $products = $factory->createMany($db->getProducts());

        // When Post received, delete the received products from DB
        if (isset($_POST["deleteSKUs"])){
            foreach ($_POST["deleteSKUs"] as $SKU){
                $products[$SKU]->deleteProduct();
            }
        }

    /**
     * Decides what should be shown before the Attribute
     *
     * @param $type
     * @return string
     */
    function atrPrefix($type){
            switch ($type){
                case "Book":
                    return "Weight: ";
                case "CD":
                    return "Size: ";
                case "Furniture":
                    return "Dimensions: ";
            }
        }

        // Display all products
        foreach ($products as $SKU => $product) {
            $properties = $product->getProperties();
            ?>
            <div id="<?php echo $properties["SKU"] ?>" class="item property_container">
                <input class="float-left property_checkbox" type="checkbox" value="<?php echo $properties["SKU"] ?>">
                <div class="property"><?php echo $properties["SKU"] ?></div>
                <div class="property"><?php echo $properties["Name"] ?></div>
                <div class="property"><?php echo $properties["Price"] . " $"  ?></div>
                <div class="property"><?php echo atrPrefix($properties["Type"]) . $properties["Attribute"] ?></div>
            </div>
            <?php
        }
    ?>
    </div>
</body>
</html>