<?php
    include_once "../../includes/DatabaseProvider.php";

    if(isset($_POST["submit"])){
        $db = DatabaseProvider::getConnection();
        $successful = $db->addProduct($_POST["SKU"], $_POST["Name"], $_POST["Price"], $_POST["Type"], $_POST["Attribute"]);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add product</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet">
    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/new.js"></script>
</head>
<body>
    <!--Nav Bar-->
    <nav class="navbar">
        <div class="float-left">
            <a class="navbar-brand" href="list.php">Product List</a>
            <a class="navbar-brand active" href="new.php">Add Product</a>
        </div>
        <div class="float-right">
            <button class="nav_button" form="form" name="submit">Save</button>
        </div>
    </nav>

    <!-- Input Form -->
    <div class="form_container">
        <form id="form" class="field_container" name="addProductForm" action="new.php" method="POST" onsubmit="return validateForm(event)">
            <div class="properties">
                <!--SKU-->
                <div class="input-group">
                    <label for="SKU">SKU</label>
                    <div class="input-group-prepend">
                        <span id="SKUPrefix" class="input-group-text">--</span>
                    </div>

                    <input id="SKU" class="input" type="text" placeholder="SKU" name="SKU" required>
                </div>
                <!--NAME-->
                <div class="input-group">
                    <label for="Name">Name</label>
                    <input id="Name" class="input" type="text" placeholder="Name" name="Name" required>
                </div>
                <!--Price-->
                <div class="input-group">
                    <label for="Price">Price</label>
                    <input id="Price" class="input" type="text" placeholder="Price" name="Price" required>
                </div>
                <!--Type-->
                <div class="input-group">
                    <label for="Type">Type</label>
                    <select id="Type" class="input" name="Type" required onchange="updateAttributes()">
                        <option disabled selected>Select Type</option>
                        <option value="Book">Book</option>
                        <option value="CD">CD</option>
                        <option value="Furniture" >Furniture</option>
                    </select>
                </div>
            </div>
            <!--Attribute-->
            <div class="book">
                <!--Book-->
                <div class="input-group">
                    <label for="Weight">Weight</label>
                    <input id="Weight" class="input" type="text" placeholder="Weight" name="Attribute" required>
                    <div class="input-group-append">
                        <span class="input-group-text" id="AttrSuffix">Kg</span>
                    </div>
                </div>
                <h5 class="instructions">Please write the weight of the book in Kg</h5>
            </div>
            <div class="cd">
                <!--CD-->
                <div class="input-group">
                    <label for="Size">Size</label>
                    <input id="Size" class="input" type="text" placeholder="Size" name="Attribute" required>
                    <div class="input-group-append">
                        <span class="input-group-text" id="AttrSuffix">MB</span>
                    </div>
                </div>
                <h5 class="instructions">Please write the size of the CD in MB</h5>
            </div>
            <div class="furniture">
                <!--Furniture-->
                <div class="input-group">
                    <label for="Height">Dimensions</label>
                    <div class="input-group-prepend">
                        <span class="input-group-text">HxWxL​</span>
                    </div>
                    <input id="Height" class="input" type="text" placeholder="H" name="H" required>
                    <input id="Width" class="input" type="text" placeholder="W" name="W" required>
                    <input id="Length" class="input" type="text" placeholder="L" name="L" required>
                    <input type="hidden" name="Attribute">
                    <div class="input-group-append">
                        <span class="input-group-text">CM</span>
                    </div>
                </div>
                <h5 class="instructions">Please write Height Width and Length in their appropriate fields</h5>
            </div>
        </form>
    </div>

</body>
</html>